import { axiosInstance, apiConstants } from "../managers/axiosHandler.js";

function signUp(signUpData) {
  return new Promise((resolve, reject) => {
    axiosInstance()
      .post(apiConstants.api_auth_hostname + "registration/", signUpData)
      .then((response) => {
        resolve(response?.data);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

function login(loginData) {
  return new Promise((resolve, reject) => {
    axiosInstance()
      .post(apiConstants.api_auth_hostname + "login/", loginData)
      .then((response) => {
        resolve(response?.data);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

export { signUp, login };
