import { axiosInstance, endpoints } from "../managers/axiosHandler.js";

function createRating(ratingDict) {
  return new Promise((resolve, reject) => {
    axiosInstance()
      .post(endpoints.rating, ratingDict)
      .then((response) => {
        resolve(response?.data);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

export { createRating };
