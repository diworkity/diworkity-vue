import { axiosInstance, endpoints } from "../managers/axiosHandler.js";

function searchCompany(searchDict) {
  return new Promise((resolve, reject) => {
    axiosInstance()
      .get(endpoints.company, { params: searchDict })
      .then((response) => {
        resolve(response?.data);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

function createCompany(companyDict) {
  return new Promise((resolve, reject) => {
    axiosInstance()
      .post(endpoints.office, companyDict)
      .then((response) => {
        resolve(response?.data);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

function updateLogo(id, file) {
  return new Promise((resolve, reject) => {
    let formData = new FormData();
    formData.append("logo", file);
    axiosInstance()
      .patch(endpoints.company + id + "/", formData)
      .then((response) => {
        resolve(response?.data);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

function fetchCompany(id) {
  return new Promise((resolve, reject) => {
    axiosInstance()
      .get(endpoints.company + id + "/", null)
      .then((response) => {
        resolve(response?.data);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

function getReviews(id, filterDict) {
  return new Promise((resolve, reject) => {
    axiosInstance()
      .get(endpoints.company + id + "/ratings/", { params: filterDict })
      .then((response) => {
        resolve(response?.data);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

export { searchCompany, createCompany, fetchCompany, getReviews, updateLogo };
