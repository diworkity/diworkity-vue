import store from "../../store";
import axios from "axios";
import VueAxios from "vue-axios";

import { app } from "../../main.js";

function setupAxiosHandler() {
  app.use(VueAxios, axios);
  app.axios.defaults.xsrfCookieName = "csrftoken";
  app.axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
  app.axios.interceptors.request.use(function (config) {
    if (store.state.authStore.authToken) {
      config.headers.Authorization = "Token " + store.state.authStore.authToken;
    }
    return config;
  });
}

function axiosInstance() {
  return app.axios;
}

const apiConstants = {
  api_hostname: "http://127.0.0.1:8000/rest-api/",
  api_auth_hostname: "http://127.0.0.1:8000/rest-auth/",
  hostname: "http://127.0.0.1:8000/",
};

const endpoints = {
  company: apiConstants.hostname + "rest-api/company/",
  office: apiConstants.hostname + "rest-api/office/",
  rating: apiConstants.hostname + "rest-api/rating/",
};

export { setupAxiosHandler, axiosInstance, apiConstants, endpoints };
