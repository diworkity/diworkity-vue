const state = {
  authToken: null,
};

const getters = {
  isLoggedIn: (state) => {
    return state.authToken != null;
  },
};

const actions = {
  setAuthToken(context, authToken) {
    context.commit("SET_AUTH_TOKEN", authToken);
  },
};

const mutations = {
  SET_AUTH_TOKEN(state, authToken) {
    state.authToken = authToken;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
