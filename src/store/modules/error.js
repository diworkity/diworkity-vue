const state = {
    errors: [],
  };
  
  const getters = {
    getError: (state) => {
      return state.errors[state.errors.length - 1];
    },
  };
  
  const actions = {
    addError(context, error) {
      context.commit("ADD_ERROR", error);
    },
    removeError(context) {
      context.commit("REMOVE_ERROR");
    },
  };
  
  const mutations = {
    ADD_ERROR(state, error) {
      state.errors.push(error);
    },
    REMOVE_ERROR(state) {
      state.errors.pop();
    }
  };
  
  export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
  };
  