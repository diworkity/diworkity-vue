import { createStore } from "vuex";
import authStore from "./modules/auth.js";
import errorStore from "./modules/error.js"
import _ from "lodash";
import createPersistedState from "vuex-persistedstate";

export const initialStoreModules = {
  authStore,
  errorStore,
};

export default createStore({
  state: {},
  mutations: {
    RESET_STATE(state) {
      _.forOwn(initialStoreModules, (value, key) => {
        state[key] = _.cloneDeep(value.state);
      });
    },
  },
  actions: {
    logout(context) {
      context.commit("RESET_STATE");
    },
  },
  modules: _.cloneDeep(initialStoreModules),
  plugins: [createPersistedState()],
});
