import { createRouter, createWebHistory } from "vue-router";
import Home from "../views/Home.vue";
import SignUp from "../views/SignUp.vue";
import VerifyEmailSent from "../views/VerifyEmailSent.vue";
import Login from "../views/Login.vue";
import AddCompany from "../views/AddCompany.vue";
import SearchResults from "../views/SearchResults.vue";
import Company from "../views/Company.vue";

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/SignUp",
    name: "SignUp",
    component: SignUp,
  },
  {
    path: "/Login",
    name: "Login",
    component: Login,
  },
  {
    path: "/VerifyEmailSent",
    name: "VerifyEmailSent",
    component: VerifyEmailSent,
  },
  {
    path: "/AddCompany",
    name: "AddCompany",
    component: AddCompany,
  },
  {
    path: "/SearchResults/:searchTerm?",
    name: "SearchResults",
    component: SearchResults,
  },
  {
    path: "/Company/:id",
    name: "Company",
    component: Company,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
